# Recoil tv shows mini app

Technologies :

1. React

2. Recoil

3. React Router 5

### How to run it: 

```
git clone https://gitlab.com/estefanodi/react-recoil-show-search
cd recoil-tv-shows-example
npm install
npm start
```

### Description:

Mini app to search tv shows from an external api using Recoil as a state manager.

### You can try it at :

[surge](http://react-recoil-shows-app.surge.sh/)
