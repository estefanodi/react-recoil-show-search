import { atom, selectorFamily } from "recoil";
import axios from "axios";

export const tagsState = atom({
  key: "tagsState",
  default: [
    "batman",
    "superman",
    "hulk",
    "super mario",
    "magnum pi",
    "a-team",
    "jessica jones",
    "luke cage",
    "daredevil",
    "spongebob",
    "flash",
    "big bang theory",
    "friends",
    "how i met your mother"
  ],
});

export const titleState = atom({
  key: "titleState",
  default: "",
});

export const resultState = atom({
  key: "resultState",
  default: [],
});

export const selectedState = atom({
  key: "selectedState",
  default: null,
});

export const singleShowState = selectorFamily({
  key: "singleShowState",
  get: (showName) => async () => {
    return await getShow(showName);
  },
});

const getShow = async (showName) => {
  try {
    const { data } = await axios.get(
      `http://api.tvmaze.com/singlesearch/shows?q=${showName}`
    );
    return data;
  } catch (error) {
    console.log(error);
  }
};
