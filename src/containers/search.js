import React from "react";
import axios from "axios";
import { Tag } from "antd";
import { v4 as uuidv4 } from "uuid";

import { useRecoilState, useSetRecoilState, useRecoilValue } from "recoil";

import { titleState, resultState, tagsState } from "../utils/state";


const Search = (props) => {
  const [title, setTitle] = useRecoilState(titleState);
  const setResult = useSetRecoilState(resultState);
  const tags = useRecoilValue(tagsState);

  const handleSubmit = async (e, tag) => {
    e.preventDefault();
    try {
      const { data } = await axios.get(
        `https://api.tvmaze.com/search/shows?q=${title}`
      );
      data.length === 0 && alert("Sorry not show found");
      setResult([...data.filter((ele) => ele.show.image)]);
      setTitle("");
      props.history.push({ pathname: "/result" });
    } catch (error) {
      props.history.push("/");
      console.log(error);
    }
  };

  return (
    <div className="form_container">
      <form onSubmit={handleSubmit}>
        <input
          placeholder="Search for a tv show"
          value={title}
          type={"text"}
          onChange={(e) => setTitle(e.target.value)}
        />
        <button className="form_button" type="submit">
          Search
        </button>
      </form>
      <div className="tags_container">
        {tags.map((tag) => (
          <div className="tag">
            <Tag
              key={uuidv4()}
              color={"geekblue"}
              onClick={() => {
                setTitle(tag);
              }}
            >
              {tag}
            </Tag>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Search;
